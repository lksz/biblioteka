import {fakeAsync, flush, TestBed} from '@angular/core/testing';

import {ToastService} from './toast.service';
import {ToastController} from '@ionic/angular';
import {ToastControllerMock} from '../../../mocks/toast-controller.mock';

describe('ToastService', () => {
  let service: ToastService;
  let toastController: ToastController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        {provide: ToastController, useClass: ToastControllerMock}
      ],
      declarations: [],
      schemas: []
    });

    service = TestBed.get(ToastService);
    toastController = TestBed.get(ToastController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should create a toast', () => {
    const createSpy = spyOn(toastController, 'create').and.callThrough();
    service.show('test');
    expect(createSpy).toHaveBeenCalled();
  });
});
