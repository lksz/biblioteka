import { Injectable } from '@angular/core';
import {timer} from 'rxjs';
import {ToastController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  constructor(private toastController: ToastController) {}

  public async show(message: string) {
    const toastController = await this.toastController.create({
      header: message,
      position: 'bottom',
      buttons: [
        {text: 'Ok', role: 'cancel'}
      ]
    });
    await toastController.present();
    timer(4000).subscribe(() => toastController.dismiss());
  }
}
