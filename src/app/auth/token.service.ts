import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  private jwt = new JwtHelperService();

  public decodeToken(token: string)  {
    try {
      if (!!token) {
        return this.jwt.decodeToken(token);
      }
    } catch (e) {
      localStorage.removeItem(environment.storageName);
      return null;
    }
  }
}
