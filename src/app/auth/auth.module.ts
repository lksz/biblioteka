import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {AuthPageRoutingModule} from './auth-routing.module';

import {AuthPage} from './auth.page';
import {NgxsModule} from '@ngxs/store';
import {AuthState} from './store/auth.store';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AuthPageRoutingModule,
        ReactiveFormsModule,
        NgxsModule.forFeature([AuthState])
    ],
    declarations: [AuthPage]
})
export class AuthPageModule {
}
