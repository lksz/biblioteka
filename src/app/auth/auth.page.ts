import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {Select, Store} from '@ngxs/store';
import {AuthState, SetLoading, ToggleMode} from './store/auth.store';
import {filter, switchMap, tap} from 'rxjs/operators';
import {ToastService} from '../utils/toast.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit, OnDestroy {
  public credentialsForm: FormGroup;
  @Select(AuthState.isLoginMode) isLoginMode: Observable<boolean>;
  @Select(AuthState.isLoading) isLoading: Observable<boolean>;

  private subscription = new Subscription();

  constructor(
    private store: Store,
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private toastService: ToastService) {
  }

  ngOnInit() {
    this.subscription.add(
      this.isLoginMode
        .pipe(
          tap((isLogin) => {
            this.credentialsForm = isLogin ? this.getLoginForm() : this.getRegistrationForm();
          })
        )
        .subscribe()
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public onSubmitClick() {
    const user = this.credentialsForm.value;
    delete user.termsAccepted;

    this.store.dispatch(new SetLoading(true));

    // login
    this.subscription.add(
    this.isLoginMode
      .pipe(
        filter((isLogin) => !!isLogin),
        switchMap(() => {
          return this.authService.login(user);
        })
      )
      .subscribe((v) => {
        this.toastService.show('Zalogowano pomyślnie');
        this.router.navigateByUrl('/home');
        this.store.dispatch(new SetLoading(false));
        this.credentialsForm.reset();
      }, (err) => {
        this.toastService.show('Wystąpił błąd :( Spróbuj ponownie później.');
        this.store.dispatch(new SetLoading(false));
      })
    );

    // register
    this.subscription.add(
    this.isLoginMode
      .pipe(
        filter((isLogin) => !isLogin),
        switchMap(() => {
          return this.authService.register(user);
        })
      )
      .subscribe((v) => {
        this.toastService.show('Konto zostało stworzone, teraz możesz się zalogować!');
        this.toggleState();
        this.store.dispatch(new SetLoading(false));
        this.credentialsForm.reset();
      }, (err) => {
        this.toastService.show('Wystąpił błąd :( Spróbuj ponownie później.');
        this.store.dispatch(new SetLoading(false));
      })
    );
  }

  public toggleState() {
    this.store.dispatch(new ToggleMode());
  }

  private getLoginForm() {
    return this.formBuilder.group({
      email: ['', {validators: [Validators.email, Validators.required], updateOn: 'blur'}],
      password: ['', {validators: [Validators.required], updateOn: 'change'}]
    });
  }

  private getRegistrationForm() {
    return this.formBuilder.group({
      firstName: ['', {validators: [Validators.required], updateOn: 'blur'}],
      lastName: ['', {validators: [Validators.required], updateOn: 'blur'}],
      email: ['', {validators: [Validators.email, Validators.required], updateOn: 'blur'}],
      password: ['', {validators: [Validators.required], updateOn: 'blur'}],
      termsAccepted: [false, {validators: [Validators.requiredTrue]}]
    });
  }
}
