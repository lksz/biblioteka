import {TestBed} from '@angular/core/testing';

import {AuthService} from './auth.service';
import {NgxsModule, Store} from '@ngxs/store';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TokenService} from './token.service';
import {RouterTestingModule} from '@angular/router/testing';
import {environment} from '../../environments/environment';
import {TokenServiceMock} from '../../../mocks/token-service.mock';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MockComponent} from '../../../mocks/mock.component';
import {ToastService} from '../utils/toast.service';
import {ToastServiceMock} from '../../../mocks/toast-service.mock';

describe('AuthService', () => {
  let service: AuthService;
  let httpMock: HttpTestingController;
  let store: Store;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NgxsModule.forRoot([]),
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([
          {path: 'home', component: MockComponent}
        ])
      ],
      providers: [
        {provide: TokenService, useClass: TokenServiceMock},
        {provide: ToastService, useClass: ToastServiceMock},
        AuthService
      ],
      declarations: [MockComponent],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    });

    service = TestBed.get(AuthService);
    httpMock = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('register', () => {
    it('should send correct post', () => {
      const user = {
        firstName: 'Abc',
        lastName: 'Xyz',
        email: 'abc@xyz.pl',
        password: 'password'
      };

      service.register(user).subscribe();

      const req = httpMock.expectOne(`${environment.apiUrl}/register`, 'call to api');
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toBe(user);
    });
  });

  describe('login', () => {
    it('should send correct post', () => {
      const setItemSpy = spyOn(localStorage, 'setItem').and.callThrough();
      const user = {
        firstName: 'Abc',
        lastName: 'Xyz',
        email: 'abc@xyz.pl',
        password: 'password'
      };

      service.login(user).subscribe();

      const req = httpMock.expectOne(`${environment.apiUrl}/login`, 'call to api');
      req.flush({token: 'acb'});
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toBe(user);
      expect(setItemSpy).toHaveBeenCalled();
    });
  });

  describe('logout', () => {
    it('should remove local storage', () => {
      const removeItemSpy = spyOn(localStorage, 'removeItem').and.callThrough();
      service.logout();
      expect(removeItemSpy).toHaveBeenCalled();
    });
  });

  describe('autoLogin', () => {
    it('should dispatch the action with the current user', () => {
      spyOn(localStorage, 'getItem').and.returnValue('some data');
      const dispatchSpy = spyOn(store, 'dispatch').and.callThrough();
      service.autoLogin();
      expect(dispatchSpy).toHaveBeenCalled();
    });
  });
});
