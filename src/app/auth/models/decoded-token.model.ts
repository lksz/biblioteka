import {Roles} from './roles.model';

export interface DecodedToken {
  exp: number;
  iat: number;
  roles: Roles[];
  username: string;
  id: number;
}
