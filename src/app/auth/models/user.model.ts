export interface User {
  username: string;
  isAdmin: boolean;
  id: number;
  token: string;
}
