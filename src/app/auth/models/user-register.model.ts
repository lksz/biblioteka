import {UserData} from './user-data.model';

export interface UserRegister extends UserData {
  firstName: string;
  lastName: string;
}
