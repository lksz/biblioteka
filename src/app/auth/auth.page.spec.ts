import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AuthPage } from './auth.page';
import {NgxsModule} from '@ngxs/store';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {MockComponent} from '../../../mocks/mock.component';
import {AuthService} from './auth.service';
import {AuthServiceMock} from '../../../mocks/auth-service.mock';
import {ToastService} from '../utils/toast.service';
import {ToastServiceMock} from '../../../mocks/toast-service.mock';

describe('AuthPage', () => {
  let component: AuthPage;
  let fixture: ComponentFixture<AuthPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthPage, MockComponent ],
      imports: [
        IonicModule.forRoot(),
        NgxsModule.forRoot([]),
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([
          {path: 'home', component: MockComponent}
        ])
      ],
      providers: [
        {provide: AuthService, useClass: AuthServiceMock},
        {provide: ToastService, useClass: ToastServiceMock},
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AuthPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
