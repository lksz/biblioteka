import {Action, Selector, State, StateContext} from '@ngxs/store';
import {User} from '../models/user.model';

export class SetUser {
  static readonly type = '[Auth] Set Username';
  constructor(public user: User | null) {
  }
}

export class SetLoading {
  static readonly type = '[Auth] Set isLoading';
  constructor(public loading: boolean) {
  }
}

export class ToggleMode {
  static readonly type = '[Auth] Toggle Mode';
}

export interface AuthStateModel {
  isLoginMode: boolean;
  loading: boolean;
  user: User | null;
}

@State<AuthStateModel>({
  name: 'auth',
  defaults: {
    isLoginMode: false,
    loading: false,
    user: null
  }
})
export class AuthState {
  @Selector()
  static isLoginMode(state: AuthStateModel) {
    return state.isLoginMode;
  }

  @Selector()
  static isLoading(state: AuthStateModel) {
    return state.loading;
  }

  @Selector()
  static user(state: AuthStateModel) {
    return state.user;
  }

  @Selector()
  static token(state: AuthStateModel): string | null {
    if (state.user) {
      return state.user.token;
    }

    return null;
  }

  @Action(ToggleMode)
  toggleMode(ctx: StateContext<AuthStateModel>) {
    const state = ctx.getState();
    ctx.setState({...state, isLoginMode: !state.isLoginMode});
  }

  @Action(SetLoading)
  SetLoading(ctx: StateContext<AuthStateModel>, action: SetLoading) {
    ctx.patchState({loading: action.loading});
  }

  @Action(SetUser)
  setUser(ctx: StateContext<AuthStateModel>, action: SetUser) {
    ctx.patchState({user: action.user});
  }
}
