import {TestBed} from '@angular/core/testing';
import {NgxsModule, Store} from '@ngxs/store';
import {AuthState} from './auth.store';

describe('AuthState', () => {
  let store: Store;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NgxsModule.forRoot([AuthState])
      ]
    });

    store = TestBed.get(Store);
  });

  it('should return default isLoginMode state', () => {
    const test = store.selectSnapshot(AuthState.isLoginMode);
    expect(test).toEqual(false);
  });
});
