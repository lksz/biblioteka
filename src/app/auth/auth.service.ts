import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {TokenService} from './token.service';
import {tap} from 'rxjs/operators';
import {Store} from '@ngxs/store';
import {SetUser} from './store/auth.store';
import {Router} from '@angular/router';
import {ToastService} from '../utils/toast.service';
import {DecodedToken} from './models/decoded-token.model';
import {Roles} from './models/roles.model';
import {UserData} from './models/user-data.model';
import {UserRegister} from './models/user-register.model';
import {User} from './models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private http: HttpClient,
    private tokenService: TokenService,
    private store: Store,
    private router: Router,
    private toastService: ToastService) {
  }

  public register(userData: UserRegister): Observable<any[]> {
    return this.http.post<any[]>(`${environment.apiUrl}/register`, userData);
  }

  public login(userData: UserData): Observable<{ token: string }> {
    return this.http.post<{ token: string }>(`${environment.apiUrl}/login`, userData)
      .pipe(
        tap((response) => {
          localStorage.setItem(environment.storageName, response.token);
          const decodedToken: DecodedToken = this.tokenService.decodeToken(response.token);

          const user: User = {
            username: decodedToken.username,
            isAdmin: decodedToken.roles[0] === Roles.ROLE_ADMIN,
            token: response.token,
            id: decodedToken.id
          };

          this.store.dispatch(new SetUser(user));
        })
      );
  }

  public logout() {
    localStorage.removeItem(environment.storageName);
    this.store.dispatch(new SetUser(null));
    this.router.navigateByUrl('/home');
    this.toastService.show('Wylogowano pomyślnie');
  }

  public autoLogin() {
    const storedToken = localStorage.getItem(environment.storageName);
    if (storedToken) {
      const decodedToken: DecodedToken = this.tokenService.decodeToken(storedToken);

      const currentTime = Math.floor(Date.now() / 1000);

      // token valid
      if ((currentTime - decodedToken.exp) < 0) {
        const user: User = {
          username: decodedToken.username,
          isAdmin: decodedToken.roles[0] === Roles.ROLE_ADMIN,
          token: storedToken,
          id: decodedToken.id
        };

        this.store.dispatch(new SetUser(user));
      }
    }
  }
}
