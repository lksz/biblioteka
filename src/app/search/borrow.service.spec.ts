import {TestBed} from '@angular/core/testing';

import {BorrowService} from './borrow.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {NgxsModule, Store} from '@ngxs/store';
import {MockComponent} from '../../../mocks/mock.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {environment} from '../../environments/environment';
import {of} from 'rxjs';

describe('BorrowService', () => {
  let service: BorrowService;
  let httpMock: HttpTestingController;
  let store: Store;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NgxsModule.forRoot([]),
        HttpClientTestingModule,
      ],
      providers: [],
      declarations: [MockComponent],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    });

    service = TestBed.get(BorrowService);
    httpMock = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should send correct request to borrow a book', () => {
    spyOn(store, 'select').and.returnValue(of({token: 'abc', id: 1}));
    service.borrow({} as any).subscribe();
    const req = httpMock.expectOne(`${environment.apiUrl}/api/virtual_book_accesses`, 'call to api');
    expect(req.request.method).toBe('POST');
  });

  it('should send correct request to get borrowings', () => {
    spyOn(store, 'select').and.returnValue(of({token: 'abc', id: 1}));
    service.getMyBorrowings().subscribe();
    const req = httpMock.expectOne(`${environment.apiUrl}/api/users/1/virtual_book_accesses`, 'call to api');
    expect(req.request.method).toBe('GET');
  });
});
