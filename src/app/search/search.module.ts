import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchPageRoutingModule } from './search-routing.module';

import { SearchPage } from './search.page';
import {BorrowComponent} from './borrow/borrow.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    SearchPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [SearchPage, BorrowComponent],
  entryComponents: [SearchPage, BorrowComponent]
})
export class SearchPageModule {}
