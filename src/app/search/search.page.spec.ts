import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {IonicModule, ModalController} from '@ionic/angular';

import { SearchPage } from './search.page';
import {ReactiveFormsModule} from '@angular/forms';
import {NgxsModule} from '@ngxs/store';
import {BooksService} from '../books/books.service';
import {BooksServiceMock} from '../../../mocks/book-service.mock';
import {ToastService} from '../utils/toast.service';
import {ToastServiceMock} from '../../../mocks/toast-service.mock';
import {ModalControllerMock} from '../../../mocks/modal-controller.mock';

describe('SearchPage', () => {
  let component: SearchPage;
  let fixture: ComponentFixture<SearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchPage ],
      imports: [
        IonicModule.forRoot(),
        ReactiveFormsModule,
        NgxsModule.forRoot([]),
      ],
      providers: [
        {provide: BooksService, useClass: BooksServiceMock},
        {provide: ToastService, useClass: ToastServiceMock},
        {provide: ModalController, useClass: ModalControllerMock}
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(SearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
