import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {IonicModule, ModalController} from '@ionic/angular';

import { BorrowComponent } from './borrow.component';
import {NgxsModule} from '@ngxs/store';
import {ReactiveFormsModule} from '@angular/forms';
import {ModalControllerMock} from '../../../../mocks/modal-controller.mock';
import {AuthService} from '../../auth/auth.service';
import {AuthServiceMock} from '../../../../mocks/auth-service.mock';
import {BorrowService} from '../borrow.service';
import {BorrowServiceMock} from '../../../../mocks/borrow-service.mock';
import {ToastService} from '../../utils/toast.service';
import {ToastServiceMock} from '../../../../mocks/toast-service.mock';

describe('BorrowComponent', () => {
  let component: BorrowComponent;
  let fixture: ComponentFixture<BorrowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrowComponent ],
      imports: [
        IonicModule.forRoot(),
        NgxsModule.forRoot([]),
        ReactiveFormsModule
      ],
      providers: [
        {provide: ModalController, useClass: ModalControllerMock},
        {provide: AuthService, useClass: AuthServiceMock},
        {provide: BorrowService, useClass: BorrowServiceMock},
        {provide: ToastService, useClass: ToastServiceMock},
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(BorrowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
