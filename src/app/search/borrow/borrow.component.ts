import {Component, Input, OnInit} from '@angular/core';
import {Book} from '../../books/models/book.model';
import {Select, Store} from '@ngxs/store';
import {ModalController} from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../auth/auth.service';
import {BorrowService} from '../borrow.service';
import {BooksState} from '../../books/store/books.store';
import {Observable} from 'rxjs';
import {SearchState, SetBorrowLoading} from '../store/books.store';
import {ToastService} from '../../utils/toast.service';
import {SetMyBooksLoaded} from '../../my-books/store/my-books.store';

@Component({
  selector: 'app-borrow',
  templateUrl: './borrow.component.html',
  styleUrls: ['./borrow.component.scss'],
})
export class BorrowComponent implements OnInit {
  @Input() book: Book;

  @Select(SearchState.borrowLoading) loading: Observable<boolean>;

  public borrowBookForm: FormGroup;
  public minDate: string;

  constructor(private store: Store,
              private modalCtrl: ModalController,
              private formBuilder: FormBuilder,
              private authService: AuthService,
              private borrowService: BorrowService,
              private toastService: ToastService) {
  }

  ngOnInit() {
    this.minDate = new Date().toISOString();

    this.borrowBookForm = this.formBuilder.group({
      accessStart: [null, [Validators.required]],
      accessEnd: [null, [Validators.required]]
    });
  }

  public onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }

  onSubmit() {
    this.store.dispatch(new SetBorrowLoading(true));

    const borrowData = {
      ...this.borrowBookForm.value,
      virtualBook: this.book.virtualBook
    };

    this.borrowService.borrow(borrowData)
      .subscribe(
        (v) => {
          this.store.dispatch([new SetBorrowLoading(false), new SetMyBooksLoaded(true)]);
          this.modalCtrl.dismiss();
          this.toastService.show('Książka została wypożyczona. Przejdź do \'Moje książki\' by ją zobaczyć.');
        },
        (err) => {
          this.store.dispatch([new SetBorrowLoading(false), new SetMyBooksLoaded(true)]);
          this.modalCtrl.dismiss();
          this.toastService.show('Nie udało się wypożyczyć książki. Proszę spróbuj ponownie później.');
        }
      );
  }
}
