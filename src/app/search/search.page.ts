import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Select, Store} from '@ngxs/store';
import {BooksState, SetBooks, SetLoaded, SetLoading} from '../books/store/books.store';
import {combineLatest, Observable, of} from 'rxjs';
import {Book} from '../books/models/book.model';
import {ModalController} from '@ionic/angular';
import {BooksService} from '../books/books.service';
import {ToastService} from '../utils/toast.service';
import {debounceTime, filter, map, switchMap, tap} from 'rxjs/operators';
import {BorrowComponent} from './borrow/borrow.component';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  @Select(BooksState.books) books: Observable<Book[]>;
  @Select(BooksState.loaded) loaded: Observable<boolean>;

  public searchForm: FormGroup;
  public filteredBooks: Observable<Book[]>;
  public notFound = false;

  constructor(private formBuilder: FormBuilder,
              private store: Store,
              private modalController: ModalController,
              private bookService: BooksService,
              private toastService: ToastService) {
  }

  ngOnInit() {
    this.searchForm = this.getSearchForm();
    this.updateAllBooks();

    this.filteredBooks = this.searchForm.valueChanges
      .pipe(
        debounceTime(500),
        switchMap((value: {book: string}) => {
          return combineLatest([of(value.book), this.books]);
        }),
        map(([searchedFraze, books]: [string, Book[]]): Book[] => {
          if (!searchedFraze) {
            return [];
          }

          return books.filter((book) => {
            return book.title.includes(searchedFraze);
          });
        }),
        tap((books) => {
          books.length ? this.notFound = false : this.notFound = true;
        })
      );
  }

  public async borrowABook(book: Book) {
    const modal = await this.modalController.create({
      component: BorrowComponent,
      componentProps: {
        book
      }
    });
    return await modal.present();
  }

  private getSearchForm() {
    return this.formBuilder.group({
      book: ['']
    });
  }

  private updateAllBooks() {
    this.loaded
      .pipe(
        filter((loaded) => !loaded),
        switchMap(() => {
          return this.bookService.getAllBooks();
        })
      )
      .subscribe(
        (response) => {
          this.store.dispatch([new SetBooks(response['hydra:member']), new SetLoaded(true)]);
        }, (err) => {
          this.store.dispatch([new SetLoaded(true), new SetLoading(false)]);
          this.toastService.show('Nie udało się pobrać książek :(');
        });
  }
}
