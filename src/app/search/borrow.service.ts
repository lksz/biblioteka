import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Store} from '@ngxs/store';
import {AuthState} from '../auth/store/auth.store';
import {switchMap} from 'rxjs/operators';
import {forkJoin, Observable} from 'rxjs';

// {
//   @id: '/api/virtual_book_access/1';
//   @type: 'VirtualBookAccess';
//   accessEnd: '2019-12-22T19:58:31+00:00';
//   accessStart: '2019-12-23T19:58:31+00:00';
//   id: 1;
//   owner: '/api/users/1';
//   virtualBook: '/api/virtual_books/1';
// }
interface VirtualBook {
  '@id': string;
  '@type': string;
  accessEnd: string;
  accessStart: string;
  id: number;
  owner: string;
  virtualBook: string;
}

interface MyBorrowings {
  '@context': string;
  'hydra:member': VirtualBook[];
  'hydra:totalItems': number;
}

interface BorrowData {
  accessStart: string;
  accessEnd: string;
  virtualBook: string;
}

// {
//   "virtualBook": "/api/virtual_books/4",
//   "accessStart": "2019-12-19T15:41:27.435Z",
//   "accessEnd": "2019-12-19T15:41:27.435Z",
//   "owner": "/api/users/2"
// }
interface FullBorrowData extends BorrowData {
  owner: string;
}

@Injectable({
  providedIn: 'root'
})
export class BorrowService {

  constructor(private http: HttpClient, private store: Store) {
  }

  public borrow(borrowData: BorrowData) {
    return this.store.select(AuthState.user)
      .pipe(
        switchMap((user) => {
          const httpOptions = {
            headers: new HttpHeaders({
              Authorization: `Bearer ${user.token}`
            })
          };

          const body: FullBorrowData = {
            virtualBook: borrowData.virtualBook,
            accessStart: borrowData.accessStart,
            accessEnd: borrowData.accessEnd,
            owner: `/api/users/${user.id}`
          };

          return this.http.post(`${environment.apiUrl}/api/virtual_book_accesses`, body, httpOptions);
        })
      );
  }

  public getMyBorrowings(): Observable<any[]> {
    let httpOptions;

    return this.store.select(AuthState.user)
      .pipe(
        switchMap((user) => {
          httpOptions = {
            headers: new HttpHeaders({
              Authorization: `Bearer ${user.token}`
            })
          };

          return this.http.get<MyBorrowings>(`${environment.apiUrl}/api/users/${user.id}/virtual_book_accesses`, httpOptions);

          // response
          // @context: "/api/contexts/VirtualBookAccess"
          // @id: "/api/users/1/virtual_book_accesses"
          // @type: "hydra:Collection"
          // hydra:member: (5) [{…}, {…}, {…}, {…}, {…}] <- can be duplicated
          // hydra:totalItems: 5
        }),
        switchMap((myBorrowings) => {
          const uniqueBorrowings = [...new Set(myBorrowings['hydra:member'].map((borrowing: VirtualBook) => borrowing.virtualBook))];
          const uniqueRequests = [...uniqueBorrowings.map((virtualBook: string) => this.http.get(`${environment.apiUrl}${virtualBook}`, httpOptions))];

          return forkJoin([...uniqueRequests]);

          // ask for only unique virtual books
          // [{
          // @context: "/api/contexts/VirtualBook"
          // @id: "/api/virtual_books/1"
          // @type: "VirtualBook"
          // book: "/api/books/14"
          // enabled: true
          // id: 1
          // sourceUri: "http://google.com/"
          // }]
        }),
        switchMap((virtualBooks: any[]) => {
          const booksRequests = [...virtualBooks.map((virtualBook) => this.http.get(`${environment.apiUrl}${virtualBook.book}`, httpOptions))];
          return forkJoin([...booksRequests]);
          // get finally the books we want with titles and descriptions
          // [{
            // @context: "/api/contexts/Book"
            // @id: "/api/books/14"
            // @type: "Book"
            // authors: []
            // description: "Opis"
            // id: 14
            // instances: []
            // isbn: "isbn"
            // numberOfPages: "123"
            // title: "Test 1"
            // virtualBook: "/api/virtual_books/1"
          // }]
        })
      )
      ;
  }
}
