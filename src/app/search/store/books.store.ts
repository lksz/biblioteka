import {Action, Selector, State, StateContext} from '@ngxs/store';

export class SetBorrowLoading {
  static readonly type = '[Search | Borrow] Set loading';
  constructor(public loading: boolean) {
  }
}

export interface SearchStateModel {
  borrow: {
    loading: boolean;
  };
}

@State<SearchStateModel>({
  name: 'search',
  defaults: {
    borrow: {
      loading: false
    }
  }
})
export class SearchState {
  @Selector()
  static borrowLoading(state: SearchStateModel) {
    return state.borrow.loading;
  }

  @Action(SetBorrowLoading)
  setBorrowLoading(ctx: StateContext<SearchStateModel>, action: SetBorrowLoading) {
    ctx.patchState({borrow: {loading: action.loading}});
  }
}
