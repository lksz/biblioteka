import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Store} from '@ngxs/store';
import {NewBook} from './models/new-book.model';
import {AuthState} from '../auth/store/auth.store';
import {switchMap, tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {combineLatest, Observable, of} from 'rxjs';
import {Book} from './models/book.model';

interface AddBookResponse extends Book {
  '@context': string;
}

interface AddBookInstanceResponse {
  '@context': string;
  '@id': string;
  '@type': string;
  book: string;
  borrowings: any[];
  id: number; // like 1
  inStock: boolean;
}

interface AllBooksResponse {
  'hydra:member': Book[];
  'hydra:totalItems': number;
}

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  constructor(private http: HttpClient, private store: Store) {
  }

  public addBook(book: NewBook): Observable<any> {
    return this.store.select(AuthState.token)
      .pipe(
        switchMap((token) => {
          const httpOptions = {
            headers: new HttpHeaders({
              Authorization: `Bearer ${token}`
            })
          };

          return this.http.post<AddBookResponse>(`${environment.apiUrl}/api/books`, book, httpOptions);
        })
      );
  }

  public getAllBooks(): Observable<AllBooksResponse> {
    return this.store.select(AuthState.token)
      .pipe(
        switchMap((token) => {
          const httpOptions = {
            headers: new HttpHeaders({
              Authorization: `Bearer ${token}`
            })
          };

          return this.http.get<AllBooksResponse>(`${environment.apiUrl}/api/books?page=1`, httpOptions);
        })
      );
  }
}
