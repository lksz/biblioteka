import {TestBed} from '@angular/core/testing';

import {BooksService} from './books.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NgxsModule} from '@ngxs/store';
import {MockComponent} from '../../../mocks/mock.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('BooksService', () => {
  let service: BooksService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NgxsModule.forRoot([]),
        HttpClientTestingModule,
      ],
      providers: [BooksService],
      declarations: [MockComponent],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    });

    service = TestBed.get(BooksService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
