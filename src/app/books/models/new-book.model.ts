export interface NewBook {
  title: string;
  isbn: string;
  numberOfPages: number;
  description: string;
}
