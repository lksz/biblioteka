export interface Book {
  '@id': string;
  '@type': string;
  authors: string[];
  description: string;
  id: number;
  instances: unknown[];
  isbn: string;
  numberOfPages: string;
  title: string;
  virtualBook: string;
}
