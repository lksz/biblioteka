import {Action, Selector, State, StateContext} from '@ngxs/store';
import {Book} from '../models/book.model';

export class SetLoading {
  static readonly type = '[Books] Set loading';
  constructor(public loading: boolean) {
  }
}

export class SetLoaded {
  static readonly type = '[Books] Set loaded';
  constructor(public loaded: boolean) {
  }
}

export class SetAddBookLoading {
  static readonly type = '[Books] Set add book loading';
  constructor(public loading: boolean) {
  }
}

export class SetBooks {
  static readonly type = '[Books] Set books';
  constructor(public books: Book[]) {
  }
}

export interface BooksStateModel {
  loaded: boolean;
  loading: boolean;
  allBooks: Book[];
  addBook: {
    loading: boolean;
  };
}

@State<BooksStateModel>({
  name: 'books',
  defaults: {
    loaded: false,
    loading: false,
    allBooks: [],
    addBook: {
      loading: false
    }
  }
})
export class BooksState {
  @Selector()
  static loaded(state: BooksStateModel) {
    return state.loaded;
  }

  @Selector()
  static loading(state: BooksStateModel) {
    return state.loading;
  }

  @Selector()
  static addBookLoading(state: BooksStateModel) {
    return state.addBook.loading;
  }

  @Selector()
  static books(state: BooksStateModel) {
    return state.allBooks;
  }

  @Action(SetLoading)
  setLoading(ctx: StateContext<BooksStateModel>, action: SetLoading) {
    ctx.patchState({loading: action.loading});
  }

  @Action(SetLoaded)
  setLoaded(ctx: StateContext<BooksStateModel>, action: SetLoaded) {
    ctx.patchState({loaded: action.loaded});
  }

  @Action(SetAddBookLoading)
  setAddBookLoading(ctx: StateContext<BooksStateModel>, {loading}: SetAddBookLoading) {
    ctx.patchState({addBook: {loading}});
  }

  @Action(SetBooks)
  setBooks(ctx: StateContext<BooksStateModel>, action: SetBooks) {
    ctx.patchState({allBooks: action.books});
  }
}
