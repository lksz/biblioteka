import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {IonicModule, ModalController} from '@ionic/angular';

import { BooksPage } from './books.page';
import {NgxsModule} from '@ngxs/store';
import {ToastService} from '../utils/toast.service';
import {ToastServiceMock} from '../../../mocks/toast-service.mock';
import {BooksService} from './books.service';
import {BooksServiceMock} from '../../../mocks/book-service.mock';
import {ModalControllerMock} from '../../../mocks/modal-controller.mock';
import {throwError} from 'rxjs';

describe('BooksPage', () => {
  let component: BooksPage;
  let fixture: ComponentFixture<BooksPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BooksPage ],
      imports: [
        IonicModule.forRoot(),
        NgxsModule.forRoot([]),
      ],
      providers: [
        {provide: ToastService, useClass: ToastServiceMock},
        {provide: BooksService, useClass: BooksServiceMock},
        {provide: ModalController, useClass: ModalControllerMock}
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(BooksPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show error notification when getAllBooks return error', () => {
    const showSpy = spyOn(TestBed.get(ToastService), 'show').and.callThrough();
    spyOn(TestBed.get(BooksService), 'getAllBooks').and.returnValue(throwError('error'));
    component.ngOnInit();
    expect(showSpy).toHaveBeenCalled();
  });

  it('should show modal', () => {
    const createSpy = spyOn(TestBed.get(ModalController), 'create').and.callThrough();
    component.addBook();
    expect(createSpy).toHaveBeenCalled();
  });
});
