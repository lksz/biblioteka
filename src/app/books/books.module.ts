import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {BooksPageRoutingModule} from './books-routing.module';

import {BooksPage} from './books.page';
import {AddBookComponent} from './add-book/add-book.component';
import {NgxsModule} from '@ngxs/store';
import {BooksState} from './store/books.store';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    BooksPageRoutingModule,
    NgxsModule.forFeature([BooksState])
  ],
  declarations: [BooksPage, AddBookComponent],
  entryComponents: [BooksPage, AddBookComponent]
})
export class BooksPageModule {
}
