import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {IonicModule, ModalController} from '@ionic/angular';

import {AddBookComponent} from './add-book.component';
import {NgxsModule, Store} from '@ngxs/store';
import {FormBuilder, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from '../../auth/auth.service';
import {BooksService} from '../books.service';
import {ToastService} from '../../utils/toast.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AuthServiceMock} from '../../../../mocks/auth-service.mock';

describe('AddBookComponent', () => {
  let component: AddBookComponent;
  let fixture: ComponentFixture<AddBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddBookComponent],
      imports: [
        IonicModule.forRoot(),
        NgxsModule.forRoot([]),
        ReactiveFormsModule,
        HttpClientModule
      ],
      providers: [
        Store,
        ModalController,
        FormBuilder,
        {provide: AuthService, useClass: AuthServiceMock},
        BooksService,
        ToastService
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AddBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
