import {Component, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../auth/auth.service';
import {BooksService} from '../books.service';
import {ToastService} from '../../utils/toast.service';
import {Select, Store} from '@ngxs/store';
import {BooksState, SetAddBookLoading, SetLoaded, SetLoading} from '../store/books.store';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.scss'],
})
export class AddBookComponent implements OnInit {
  @Select(BooksState.addBookLoading) loading: Observable<boolean>;

  public newBookForm: FormGroup;

  constructor(
    private store: Store,
    private modalCtrl: ModalController,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private bookService: BooksService,
    private toastService: ToastService) {
  }

  ngOnInit() {
    this.newBookForm = this.formBuilder.group({
      title: [null, [Validators.required]],
      isbn: [null, [Validators.required]],
      numberOfPages: [null, [Validators.required]],
      description: [null, [Validators.required]]
    });
  }

  public onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }

  public onSubmit() {
    this.store.dispatch(new SetAddBookLoading(true));

    this.bookService.addBook({...this.newBookForm.value})
      .subscribe(
        () => {
          this.store.dispatch([new SetAddBookLoading(false), new SetLoaded(false)]);
          this.modalCtrl.dismiss();
          this.toastService.show('Książka została dodana');
        },
        (err) => {
          this.store.dispatch(new SetAddBookLoading(false));
          this.modalCtrl.dismiss();
          this.toastService.show('Nie udało się dodać książki. Proszę spróbuj ponownie później.');
        }
      );
  }
}
