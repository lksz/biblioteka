import {Component, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {AddBookComponent} from './add-book/add-book.component';
import {BooksService} from './books.service';
import {Select, Store} from '@ngxs/store';
import {BooksState, SetBooks, SetLoaded, SetLoading} from './store/books.store';
import {Observable} from 'rxjs';
import {Book} from './models/book.model';
import {filter, switchMap, tap} from 'rxjs/operators';
import {ToastService} from '../utils/toast.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.page.html',
  styleUrls: ['./books.page.scss'],
})
export class BooksPage implements OnInit {
  @Select(BooksState.books) books: Observable<Book[]>;
  @Select(BooksState.loaded) loaded: Observable<boolean>;
  @Select(BooksState.loading) loading: Observable<boolean>;

  constructor(
    private store: Store,
    private modalController: ModalController,
    private bookService: BooksService,
    private toastService: ToastService) {
  }

  ngOnInit() {
    this.loaded
      .pipe(
        filter((loaded) => !loaded),
        tap(() => this.store.dispatch(new SetLoading(true))),
        switchMap(() => {
          return this.bookService.getAllBooks();
        })
      )
      .subscribe(
        (response) => {
          console.log('all books: ', response);
          this.store.dispatch([new SetBooks(response['hydra:member']), new SetLoaded(true), new SetLoading(false)]);
        }, (err) => {
          this.store.dispatch([new SetLoaded(true), new SetLoading(false)]);
          this.toastService.show('Nie udało się pobrać książek :(');
        });
  }

  public async addBook() {
    const modal = await this.modalController.create({
      component: AddBookComponent
    });
    return await modal.present();
  }
}
