import {Component, OnInit} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {AuthService} from './auth/auth.service';
import {Select} from '@ngxs/store';
import {AuthState} from './auth/store/auth.store';
import {Observable} from 'rxjs';
import {User} from './auth/models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  @Select(AuthState.user) user: Observable<User>;
  @Select(AuthState.isLoading) isLoading: Observable<boolean>;

  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Kontakt',
      url: '/contact',
      icon: 'pin'
    },
    {
      title: 'Profil',
      url: '/user-profile',
      icon: 'person'
    }
  ];

  public appAdminPages = [
    // {
    //   title: 'Użytkownicy',
    //   url: '/users',
    //   icon: 'people'
    // },
    {
      title: 'Książki',
      url: '/books',
      icon: 'book'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit(): void {
    this.authService.autoLogin();
  }

  public logout() {
    this.authService.logout();
  }

  public gotoAPI() {
    window.open('https://biblioteka-wwsi.herokuapp.com/api', '_blank');
}
}
