import {Component} from '@angular/core';
import {AuthState} from '../auth/store/auth.store';
import {Observable} from 'rxjs';
import {Select} from '@ngxs/store';
import {User} from '../auth/models/user.model';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage {
  @Select(AuthState.user) user: Observable<User>;
}
