import {Action, Selector, State, StateContext} from '@ngxs/store';
import {Book} from '../../books/models/book.model';

export class SetMyBooksLoading {
  static readonly type = '[MyBooks] Set loading';
  constructor(public loading: boolean) {
  }
}

export class SetMyBooksLoaded {
  static readonly type = '[MyBooks] Set loaded';
  constructor(public loaded: boolean) {
  }
}

export class SetMyBooks {
  static readonly type = '[MyBooks] Set My Books';
  constructor(public books: Book[]) {
  }
}

export interface MyBooksStateModel {
  loading: boolean;
  loaded: boolean;
  myBooks: Book[];
}

@State<MyBooksStateModel>({
  name: 'myBooks',
  defaults: {
    loading: false,
    loaded: false,
    myBooks: []
  }
})
export class MyBooksState {
  @Selector()
  static loading(state: MyBooksStateModel): boolean {
    return state.loading;
  }

  @Selector()
  static loaded(state: MyBooksStateModel): boolean {
    return state.loaded;
  }

  @Selector()
  static myBooks(state: MyBooksStateModel): any[] {
    return state.myBooks;
  }

  @Action(SetMyBooksLoading)
  setMyBooksLoading(ctx: StateContext<MyBooksStateModel>, action: SetMyBooksLoading) {
    ctx.patchState({loading: action.loading});
  }

  @Action(SetMyBooksLoaded)
  setMyBooksLoaded(ctx: StateContext<MyBooksStateModel>, action: SetMyBooksLoaded) {
    ctx.patchState({loaded: action.loaded});
  }

  @Action(SetMyBooks)
  setMyBooks(ctx: StateContext<MyBooksStateModel>, action: SetMyBooks) {
    ctx.patchState({myBooks: action.books});
  }
}
