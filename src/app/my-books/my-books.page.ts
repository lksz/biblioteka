import {Component} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {BorrowService} from '../search/borrow.service';
import {Observable} from 'rxjs';
import {MyBooksState, SetMyBooks, SetMyBooksLoaded, SetMyBooksLoading} from './store/my-books.store';
import {filter, switchMap, tap} from 'rxjs/operators';
import {SetLoaded, SetLoading} from '../books/store/books.store';
import {ToastService} from '../utils/toast.service';
import {Book} from '../books/models/book.model';

@Component({
  selector: 'app-my-books',
  templateUrl: './my-books.page.html',
  styleUrls: ['./my-books.page.scss'],
})
export class MyBooksPage {
  @Select(MyBooksState.loading) loading: Observable<boolean>;
  @Select(MyBooksState.loaded) loaded: Observable<boolean>;
  @Select(MyBooksState.myBooks) myBooks: Observable<Book[]>;

  public read = false;

  constructor(
    private store: Store,
    private borrowService: BorrowService,
    private toastService: ToastService) {
  }

  ionViewWillEnter() {
    this.loaded
      .pipe(
        filter((loaded) => !loaded),
        tap(() => this.store.dispatch(new SetMyBooksLoading(true))),
        switchMap(() => {
          return this.borrowService.getMyBorrowings();
        })
      )
      .subscribe(
        (response) => {
          this.store.dispatch([new SetMyBooksLoading(false), new SetMyBooks(response), new SetMyBooksLoaded(true)]);
        },
        (err) => {
          this.store.dispatch([new SetLoaded(true), new SetLoading(false)]);
          this.toastService.show('Nie udało się pobrać książek :(');
        }
      );
  }

  public toggleRead() {
    this.read = !this.read;
  }
}
