import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyBooksPage } from './my-books.page';
import {ToastService} from '../utils/toast.service';
import {ToastServiceMock} from '../../../mocks/toast-service.mock';
import {NgxsModule} from '@ngxs/store';
import {BorrowService} from '../search/borrow.service';
import {BorrowServiceMock} from '../../../mocks/borrow-service.mock';

describe('MyBooksPage', () => {
  let component: MyBooksPage;
  let fixture: ComponentFixture<MyBooksPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyBooksPage ],
      imports: [
        IonicModule.forRoot(),
        NgxsModule.forRoot([]),
      ],
      providers: [
        {provide: ToastService, useClass: ToastServiceMock},
        {provide: BorrowService, useClass: BorrowServiceMock}
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MyBooksPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
