export const environment = {
  production: true,
  apiUrl: 'https://biblioteka-wwsi.herokuapp.com',
  storageName: 'wwsiLibary'
};
