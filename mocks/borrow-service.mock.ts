import {of} from 'rxjs';

export class BorrowServiceMock {
  public borrow(borrowData: any) {
    return of({});
  }

  public getMyBorrowings() {
    return of([]);
  }
}
