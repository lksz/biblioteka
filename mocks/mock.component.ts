import {Component} from '@angular/core';

@Component({
  selector: 'app-component-mock',
  template: '<p>Mock Component</p>'
})
export class MockComponent {}
