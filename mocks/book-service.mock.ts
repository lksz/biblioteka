import {NewBook} from '../src/app/books/models/new-book.model';
import {of} from 'rxjs';

export class BooksServiceMock {
  public addBook(book: NewBook) {
    return of({});
  }

  public getAllBooks() {
    return of({});
  }
}
