export class ModalControllerMock {
  public create() {
    return {
      present: () => {
      },
      dismiss: () => {
      },
    };
  }
}
