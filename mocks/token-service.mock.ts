import {DecodedToken} from '../src/app/auth/models/decoded-token.model';

export class TokenServiceMock {
  public decodeToken(data: any): DecodedToken {
    return {
      exp: 9999999999999999,
      iat: 2,
      roles: [],
      username: 'Abc',
      id: 1
    };
  }
}
