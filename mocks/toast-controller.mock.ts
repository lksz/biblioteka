export class ToastControllerMock {
  public create() {
    return {
      present: () => {
      },
      dismiss: () => {
      },
    };
  }
}
