import {Observable, of} from 'rxjs';
import {UserRegister} from '../src/app/auth/models/user-register.model';
import {UserData} from '../src/app/auth/models/user-data.model';

export class AuthServiceMock {
  public register(userData: UserRegister): Observable<any[]> {
    return of([]);
  }

  public login(userData: UserData): Observable<{ token: string }> {
    return of({token: ''});
  }

  public logout() {}

  public autoLogin() {}

}
